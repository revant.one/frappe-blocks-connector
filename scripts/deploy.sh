#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_FC=$(helm ls -q stg-frappe-connector --tiller-namespace building-blocks-7336983)
if [ "$CHECK_FC" = "stg-frappe-connector" ]
then
    echo "Updating existing stg-frappe-connector . . ."
    helm upgrade stg-frappe-connector \
        --tiller-namespace building-blocks-7336983 \
        --namespace building-blocks-7336983 \
        --reuse-values \
        --recreate-pods \
        helm-charts/frappe-blocks-connector
fi
