// vue.config.js
module.exports = {
    devServer: {
        proxy: {
            "/info": { "target": "http://fcon.localhost:3300", "secure": false, "logLevel": "debug" },
        },
    }
}