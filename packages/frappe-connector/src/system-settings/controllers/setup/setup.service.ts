import { Injectable, HttpService } from '@nestjs/common';
import { switchMap } from 'rxjs/operators';
import { settingsAlreadyExists } from '../../../constants/exceptions';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { COMMUNICATION_SERVER } from '../../../constants/app-strings';
import { of } from 'rxjs';

@Injectable()
export class SetupService {
  setupData: { [key: string]: any } = {};
  constructor(
    protected readonly serverSettingsService: ServerSettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.serverSettingsService.count()) {
      throw settingsAlreadyExists;
    }

    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .pipe(
        switchMap(openIdConf => {
          this.setupData = openIdConf.data;
          return this.http.get(params.authServerURL + '/info');
        }),
        switchMap(authInfo => {
          const communicationService = authInfo.data.services.filter(
            service => service.type === COMMUNICATION_SERVER,
          );
          if (communicationService.length > 0) {
            this.setupData.communicationService = communicationService[0].url;
          }
          return of(this.setupData);
        }),
      )
      .subscribe({
        next: async response => {
          params.authorizationURL = response.authorization_endpoint;
          params.tokenURL = response.token_endpoint;
          params.profileURL = response.userinfo_endpoint;
          params.revocationURL = response.revocation_endpoint;
          params.introspectionURL = response.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          params.communicationService = response.communicationService;
          await this.serverSettingsService.save(params);
        },
        error: error => {
          // TODO : Log error
        },
      });
  }

  async getInfo() {
    const info = await this.serverSettingsService.find();
    if (info) {
      delete info.clientSecret, info._id;
    }
    return info;
  }
}
