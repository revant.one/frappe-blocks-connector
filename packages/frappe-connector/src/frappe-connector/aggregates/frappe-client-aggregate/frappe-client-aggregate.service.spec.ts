import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/common';
import { FrappeClientAggregateService } from './frappe-client-aggregate.service';
import { FrappeClientService } from '../../entities/frappe-client/frappe-client.service';
import { FrappeTokenService } from '../../entities/frappe-token/frappe-token.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

describe('Oauth2ProviderAggregateService', () => {
  let service: FrappeClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: FrappeClientAggregateService,
          useValue: {},
        },
        { provide: FrappeClientService, useValue: {} },
        { provide: FrappeTokenService, useValue: {} },
        { provide: HttpService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
      ],
    }).compile();

    service = module.get<FrappeClientAggregateService>(
      FrappeClientAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
