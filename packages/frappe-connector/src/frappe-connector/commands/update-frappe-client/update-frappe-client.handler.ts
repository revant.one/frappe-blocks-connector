import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateFrappeClientCommand } from './update-frappe-client.command';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@CommandHandler(UpdateFrappeClientCommand)
export class UpdateFrappeClientHandler
  implements ICommandHandler<UpdateFrappeClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: FrappeClientAggregateService,
  ) {}

  async execute(command: UpdateFrappeClientCommand) {
    const { providerUuid, payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateProvider(providerUuid, payload);
    aggregate.commit();
  }
}
