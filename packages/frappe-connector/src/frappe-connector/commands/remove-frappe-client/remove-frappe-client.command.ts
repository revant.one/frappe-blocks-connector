import { ICommand } from '@nestjs/cqrs';

export class RemoveFrappeClientCommand implements ICommand {
  constructor(
    public readonly providerUuid: string,
    public readonly actorUuid: string,
  ) {}
}
