import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { VerifyClientConnectionQuery } from './verify-client-connection.query';
import { FrappeTokenManagerService } from '../../aggregates/frappe-token-manager/frappe-token-manager.service';
import { BadRequestException } from '@nestjs/common';
import {
  INVALID_USER,
  INVALID_FRAPPE_CLIENT,
} from '../../../constants/messages';

@QueryHandler(VerifyClientConnectionQuery)
export class VerifyClientConnectionHandler
  implements IQueryHandler<VerifyClientConnectionQuery> {
  constructor(private manager: FrappeTokenManagerService) {}

  async execute(query: VerifyClientConnectionQuery) {
    const { providerUuid, userUuid } = query;
    if (!userUuid) throw new BadRequestException(INVALID_USER);
    if (!providerUuid) throw new BadRequestException(INVALID_FRAPPE_CLIENT);
    const token = await this.manager.verifyClientConnection(
      providerUuid,
      userUuid,
    );
    const isConnected = token ? true : false;
    return { isConnected };
  }
}
