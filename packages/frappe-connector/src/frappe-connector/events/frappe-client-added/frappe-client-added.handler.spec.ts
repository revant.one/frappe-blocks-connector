import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { FrappeClientAddedHandler } from './frappe-client-added.handler';
import { FrappeClientAddedEvent } from './frappe-client-added.event';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';

describe('Event: FrappeClientAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: FrappeClientAddedHandler;
  const mockProvider = new FrappeClient();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        FrappeClientAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<FrappeClientAddedHandler>(
      FrappeClientAddedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    mockProvider.save = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new FrappeClientAddedEvent(mockProvider));
    expect(mockProvider.save).toHaveBeenCalledTimes(1);
  });
});
