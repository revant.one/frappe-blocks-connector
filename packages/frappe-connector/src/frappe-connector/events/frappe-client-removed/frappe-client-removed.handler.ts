import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { FrappeClientRemovedEvent } from './frappe-client-removed.event';

@EventsHandler(FrappeClientRemovedEvent)
export class FrappeClientRemovedHandler
  implements IEventHandler<FrappeClientRemovedEvent> {
  handle(event: FrappeClientRemovedEvent) {
    const { client: provider } = event;
    provider
      .remove()
      .then(success => {})
      .catch(error => {});
  }
}
